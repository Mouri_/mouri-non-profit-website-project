function emailValidation(event) {
  const email = document.getElementById("email-id").value;
  const button = document.getElementById("subscribe-button");
  const input = document.getElementById("email-id");
  if (email == "") {
    input.placeholder = "Please enter email address";
    input.className = "red";
    event.preventDefault();
  } else if (!validEmail(email)) {
    button.innerHTML = "Invlid email";
    button.style = "background:  #ff0033;";
    input.className = "red";
    event.preventDefault();
    setTimeout(() => {
      button.innerHTML = "Subscribe";
      button.style = "background:  #354463;";
      input.className = "";
    }, 4000);
  }
}
function validEmail(email) {
  const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  const res = email.match(pattern);
  console.log(res);
  return res;
}
