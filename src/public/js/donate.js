function validateForm(event) {
  const firstName = document.getElementsByClassName("f-name");
  const lastName = document.getElementsByClassName("l-name");
  const address = document.getElementById("address");
  const city = document.getElementsByClassName("city");
  const state = document.getElementsByClassName("state");
  const zip = document.getElementsByClassName("zip");
  const email = document.getElementById("email-id");
  const credit = document.getElementsByClassName("credit");
  const mm = document.getElementsByClassName("mm");
  const yyyy = document.getElementsByClassName("yyyy");
  const cvc = document.getElementsByClassName("cvc");

  const button = document.getElementById("paybtn");

  const errorMessage = document.getElementById("error-message");

  validateNumber(button, cvc, event, errorMessage, "CVC", 3);
  validateNumber(
    button,
    yyyy,
    event,
    errorMessage,
    "Expiration year",
    4,
    [2021, 2100]
  );
  validateNumber(
    button,
    mm,
    event,
    errorMessage,
    "Expiration month",
    2,
    [1, 12]
  );
  validateNumber(button, credit, event, errorMessage, "Credit card number", 16);
  validateEmail(button, email, event, errorMessage);
  validateNumber(button, zip, event, errorMessage, "ZIP", 6);
  validateName(button, state, event, errorMessage, "State");
  validateName(button, city, event, errorMessage, "City");
  validateAddress(button, address, event, errorMessage);
  validateName(button, lastName, event, errorMessage, "Last");
  validateName(button, firstName, event, errorMessage, "First");
}

function validateNumber(button, num, event, errorMessage, msg, length, range) {
  if (num[0].value == "") {
    lineRed(num[0], `Please enter ${msg}`, event);
    codeRed(errorMessage, `Please enter ${msg}`, event);
    changeButton(button);
    return false;
  }
  if (num[0].value.length != length) {
    lineRed(num[0], `Please enter ${msg}`, event);
    codeRed(errorMessage, `${msg} should be ${length} characters`, event);
    changeButton(button);
    return false;
  }
  if (range != undefined) {
    if (num[0].value < range[0] || num[0].value > range[1]) {
      lineRed(
        num[0],
        `${msg} should be in range of ${range[0]} and ${range[1]}`,
        event
      );
      codeRed(
        errorMessage,
        `${msg} should be in range of ${range[0]} and ${range[1]}`,
        event
      );
      changeButton(button);
      return false;
    }
  }
}

function validateName(button, name, event, errorMessage, msg) {
  if (name[0].value == "") {
    lineRed(name[0], `Please enter ${msg} name`, event);
    codeRed(errorMessage, `Please enter ${msg} name`, event);
    changeButton(button);
    return false;
  }
  const pattern = /^[a-z ,.'-]+$/i;
  if (!name[0].value.match(pattern)) {
    lineRed(name[0], "", event);
    codeRed(errorMessage, `Please enter a valid ${msg} name`, event);
    changeButton(button);
    return false;
  }
}

function validateAddress(button, address, event, errorMessage) {
  if (address.value == "") {
    lineRed(address, "Please enter an address", event);
    codeRed(errorMessage, "Please enter an address", event);
    changeButton(button);
    return false;
  }
  if (address.value.length < 10) {
    lineRed(address, "", event);
    codeRed(
      errorMessage,
      "Please enter a detailed address (at least 10 characters)",
      event
    );
    changeButton(button);
    return false;
  }
}

function validateEmail(button, email, event, errorMessage) {
  if (email.value == "") {
    lineRed(email, "Please enter email address", event);
    codeRed(errorMessage, "Please enter an email", event);
    changeButton(button);
    return false;
  }
  const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  if (!email.value.match(pattern)) {
    lineRed(email, "Please enter email address", event);
    codeRed(errorMessage, "Please enter a valid email", event);
    changeButton(button);
    return false;
  }
}

function changeButton(button) {
  button.innerHTML = "Please correct the errors";
  button.style = "color: #ffff; background: #ff0033";
  setTimeout(() => {
    button.innerHTML = "Process Payment";
    button.style = "background:  #c1f7d5;";
    input.className = "";
  }, 4000);
}

function lineRed(element, message, event) {
  element.style = "border-bottom: 2px solid #ff0033;";
  element.placeholder = message;
  event.preventDefault();
}

function codeRed(element, message, event) {
  element.innerHTML = message;
  element.style = "display: unset";
  event.preventDefault();
}
